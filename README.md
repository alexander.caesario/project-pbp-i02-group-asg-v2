# workspace_manager


# Project PBP I02 Group Assignment


## Group members:
- Indira Devi Rusvandy (2006488732)
- Benedictus Jevan Winata (2006519870)
- Bryant Tanujaya (2006519896)
- Alexander Caesario Bangun (2006519851)
- Ajie Restu Sanggusti (2006490081)
- Muhammad Aaqil Abdullah (2006489501)
- Nicholas Jonathan Kinandana (2006490106)

## Project Description:

Stotes is a workspace web application that helps organizing your daily tasks and makes sure
your remote work gets finished on time. Create projects and invite your team members to keep you guys all on the same page. 

Users can sign in to their accounts, or register a new one. Then, they will be directed to the homepage where they can either start a new project, or look at some of their recent projects and tasks. To boost productivity, there is a newsfeed centering around how to be more productive. 

When a user creates a new project, they can opt to add others to join as members of the workspace, as well as setting up a timeframe when the project is due. Next comes the fun part—tasks! Creators of the workspace can start adding tasks with their: deadline, PIC, type, and description. Creators can also create meetings, that state the date and time as well as the link to the meeting.

Users can also join existing projects as members, simply by searching the projectID and inputing the passcode. 



## Modules:
1.  Home: 
    This is the landing page for Stotes, which will display only a newsfeed as well as a prompt to sign in or register for non-signed in users. For signed in users, it will display the newsfeed, but also at most their two last active projects and all of their tasks for the week.
2.  Accounts:
    This app serves as the authentication for non-signed in users. Users can also change or reset their passwords here.
3.  Create Project:
    This is where signed in users can finally create their projects, starting by filling out a form: name of project, members (optional), description, passcode, and timeframe of the project.
4.  Join Project
    Signed in users can join projects by searching up the ID of the project from the landing page, and entering the project passcode to start working on the project.
5.  Project Workspace
    The creator of the project can start to add/delete tasks for the members to see, as well as edit previously made tasks.
6.  Task
    Tasks can be viewed by both the creators and members. But only the creator can edit/delete the following atttributes: due date, type, description, and PIC.
7.  Meeting
    Meetings can be set up by anyone in the workspace, and they have the following attributes: date & time, description, person in meeting, meeting URL.

## Heroku app link:
[https://very-awesome-projectmanager.herokuapp.com/](https://very-awesome-projectmanager.herokuapp.com/)


## Persona
[https://docs.google.com/document/d/1BEzqtmKm02o2Ut0pXbEk_YuYWo4bHWOaxgOhjnO9kkc/edit?usp=sharing](https://docs.google.com/document/d/1BEzqtmKm02o2Ut0pXbEk_YuYWo4bHWOaxgOhjnO9kkc/edit?usp=sharing)